# SharkFest Asia Site

This repository contains the assets for sharkfestasia.wireshark.org and a GitLab CI configuration that can be used for staging and production deployments.

Changes are automatically deployed to staging.sharkfestasia.wireshark.org and must be manually deployed to sharkfestasia.wireshark.org.

## Deploying To Staging

Staging deployment is done via the “deploy_to_staging” job, which is run automatically each time a change is pushed to the “main” branch.
Deployment takes about 30 seconds.

You can re-run the job manually via the [pipelines](https://gitlab.com/wireshark/sharkfestasia-web/-/pipelines) or [jobs](https://gitlab.com/wireshark/sharkfestasia-web/-/jobs) pages.

## Deploying To Production

Production deployment is done via the “deploy_to_production” job.
It must be run manually via the [pipelines](https://gitlab.com/wireshark/sharkfestasia-web/-/pipelines) or [jobs](https://gitlab.com/wireshark/sharkfestasia-web/-/jobs) pages.
