<?php include($_SERVER[DOCUMENT_ROOT] . "/header-small.php"); ?>

<div class="col-lg-12 reg-header">
	<div class="col-lg-5 reg-header reg-image">
		
	</div>
	<div class="col-lg-7 reg-header-background">
		<h2>SharkFest<span style="letter-spacing: -5px;">'</span>18 ASIA Registration
			<p>Early bird registration is open. Attendance is limited, so register early and don’t miss out on this opportunity to learn with the best packet analysis experts on the planet.</p>
			<a target="_blank" href="http://events.eventzilla.net/e/sharkfest18-asia-2138900769">
				<button type="button" class="btn btn-primary btn-xl">Register Now!</button>
			</a>
		</h2>
		
	</div>
</div>
<div class="container">
	<div class="row">

		<div class="col-lg-10 col-centered reg-table ">
			<table class="table table-striped">
			  
			  <tbody>

			    <tr class="thead-default">
			      <th scope="row"></th>
			      <th>SharkFest Only</th>
			      <th>Wireshark U<span> Pre-Conference<br> Troubleshooting Course</span></th>
			      <th>Both</th>
			    </tr>
			  <!--
			    <tr>
			      <th>Early Bird Registration<br>(Until Feb 1st, 2018)</th>
			      <td>$600</td>
			      <td>$600</td>
			      <td>$1200</td>
			    </tr> -->
			    <tr>
			      <th>Standard Registration<br></th>
			      <td>$888</td>
			      <td>$600</td>
			      <td>$1488</td>
			    </tr>
			  </tbody>
			</table>
		</div>
		<div class="col-lg-10 col-centered">
			<div id="accordion" role="tablist" aria-multiselectable="true" class="reg-accordion">
			  <div class="card">
			    <div class="card-header" role="tab" id="headingOne">
			      <h5 class="mb-0">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			          Conference Highlights
			        </a>
			      </h5>
			    </div>

			    <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
					    <tr>
					      <th scope="row">Conference Keynote by Wireshark project founder, Gerald Combs</th>
					    </tr>
					    <tr>
					      <th scope="row">2 Days of Instructional Sessions delivered by Wireshark Experts</th>
					    </tr>
					    <tr>
					      <th scope="row">Beginner/Intermediate/Advanced/Developer Tracks</th>
					    </tr>
					    <tr>
					      <th scope="row">Welcome Dinner, Packet Challenges & Socializing</th>
					    </tr>
					    <tr>
					      <th scope="row">Sponsor Technology Showcase</th>
					    </tr>
					    <tr>
					      <th scope="row">The Packet Doctor is In! Packet Trace Analysis with the Experts</th>
					    </tr>
					  </tbody>
					</table>
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" role="tab" id="headingTwo">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			           SHARKFEST CONFERENCE DETAILS, DISCOUNTS AND CANCELLATION POLICY
			        </a>
			      </h5>
			    </div>
			    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
					    <tr>
					      <th scope="row">What is SharkFest?</th>
					      <th>SharkFest is a highly-interactive educational conference focused on sharing knowledge, experience and best practices among members of the Wireshark global developer and user communities.</th>
					    </tr>
					    <tr>
					      <th scope="row">Why Should I Attend?</th>
					      <th>
					      	
					      		• To benefit from an Immersive Wireshark Educational Experience<br>
					      		• To Interact with Veteran Packet Analysts<br>
					      		• To Test your Wireshark Chops<br>
					      		• To Network, Socialize, and Share Knowledge with Like-Minded Wireshark Enthusiasts<br>
					      		• To Enhance your Wireshark User Experience<br>
					      </th>
					    </tr>					    
					    <tr>
					      <th scope="row">What's the Schedule?</th>
					      <th>April 9th: Badge Pick-Up/Registration, Welcome Dinner<br>
								June 10th: Keynote, Sessions, Developer Den, Sponsor Technology Showcase, Packet Challenge<br>
								June 11th: Sessions, Developer Den, Packet Challenge Awards and Farewell Reception<br>
								
					    </tr>
					    <tr>
					      <th scope="row">Location</th>
					      <th>Nanyang Technological University<br>
					      		<a href="http://www.ntu.edu.sg/nec/Pages/default.aspx">Nanyang Executive Centre</a> <br>
					      	  Singapore</th>
					    </tr>
					    <tr>
					      <th scope="row">Dates</th>
					      <th>April 9th - 11th, 2018</th>
					    </tr>
					    <tr>
					      <th scope="row">Where Should I Stay?</th>
					      <th>Click <a href="lodging.php"> HERE</a> to go to the Lodging page.</th>
					    </tr>
					    <tr>
					      <th scope="row">SharkFest Registration Fee</th>
					      <th>$888 ($600 if registration is completed before Feb 2st, 2018)</th>
					    </tr>
					    <tr>
					      <th scope="row">Payment Methods</th>
					      <th>Payment for SharkFest is by credit card. Payments via Wire Transfers will incur an additional $25 or €25 fee.</th>
					    </tr>
					    <tr>
					      <th scope="row">Available SharkFest Discounts*</th>
					      <th><p>
					      		• Full-time CS students of accredited educational institutions<br>
					      		• Active-duty military personnel<br>
					      		*Please contact <a href="mailto:sharkfest@riverbed.org">sharkfest@riverbed.org</a> for discount codes and information.<br><br>
					      		
					      	  </p>
					     </th>
					    </tr>
					  </tbody>
					</table>
					
					<table class="table table-striped2 cancel-table">
					  <thead class="thead-default">
					    <tr>
					      <th colspan="2">CANCELLATION POLICY</th>
					      <th></th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td class="cancellation" scope="row">14 days or more before the SharkFest’18 ASIA Conference start date</th>
					      <td>Full Refund minus $100 Administration Fee</td>
					    </tr>
					    <tr>
					      <td class="cancellation" scope="row">Less than 14 days before the SharkFest’18 ASIA Conference start date</th>
					      <td>No Refund</td>
					    </tr>
					  </tbody>
					</table>
					<p>All cancellation requests must be made in writing to <a href="mailto:sharkfest@riverbed.com">sharkfest@riverbed.com.</a> If registered but unable to attend, another attendee within your organization may be designated to take your place at no additional charge.  All substitution requests must be submitted by the original attendee via e-mail to <a href="mailto:sharkfest@riverbed.com">sharkfest@riverbed.com.</a> On-site substitutions may be allowed if the substituting attendee provides a written request from the original attendee.
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" role="tab" id="headingThree">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			           “TROUBLESHOOTING WITH WIRESHARK” COURSE DETAILS AND CANCELLATION POLICY
			        </a>
			      </h5>
			    </div>
			    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
					    <tr>
					      <th scope="row">Dates</th>
					      <th>April 9th, 2018</th>
					    </tr>
					    <tr>
					      <th scope="row">Location</th>
					      <th>Nanyang Technological University<br>
					      		<a href="http://www.ntu.edu.sg/nec/Pages/default.aspx">Nanyang Executive Centre</a> <br>
					      	  Singapore</th>
					    </tr>
					    <tr>
					      <th scope="row">Price</th>
					      <th>$600 (includes a 1-year All Access Pass subscription)</th>
					    </tr>

					    <tr>
					      <th scope="row">Bundle w/ SharkFest</th>
					      <th>$1,200 (ends Feb. 1st, 2018)<br>
					      	$1,488 (after Feb. 1st, 2018)</th>
					    </tr>
					    <tr>
					      <th scope="row">Payment Methods</th>
					      <th>Payment is by credit card. Payments via Wire Transfers will incur an additional $25 fee.</th>
					    </tr>
					    <tr>
					      <th scope="row">More Information</th>
					      <th><a href="http://events.eventzilla.net/e/sharkfest18-asia-2138900769">	</a></th>
					    </tr>
					    <tr>
					      <th scope="row">About</th>
					      <th><p>Join Laura Chappell, Founder of Wireshark University, for a 1-day, Hands-On “Troubleshooting with Wireshark” course that precedes the start of SharkFest.
					      This course will teach you how to:</p>
					      		<ul>
					      			<li>• Find the cause of slow file transfers</li>
					      			<li>• Identify problematic infrastructure devices</li>
					      			<li>• Optimize the network</li>
					      			<li>• Measure the bandwidth use for an application or user</li>
					      			<li>• Find that needle in the haystack</li>
					      		</ul>
					      		<p>	For more information, visit <a href="http://events.eventzilla.net/e/sharkfest18-asia-2138900769"> </a> or email <a href="mailto:info@wiresharktraining.com">info@wiresharktraining.com.</p>
					      		<p>*All Discounts and a Chappell University All-Access Pass  are baked into the “Troubleshooting with Wireshark” $600 course fees. No further discounts are available.</p>
					      </th>
					    </tr>
					  </tbody>
					</table>
					
					<table class="table table-striped2 cancel-table">
					  <thead class="thead-default">
					    <tr>
					      <th colspan="2">CANCELLATION POLICY</th>
					      <th></th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td class="cancellation" scope="row">14 days or more before the “Troubleshooting with Wireshark” start date</th>
					      <td>Full Refund minus $100 administrative fee</td>
					    </tr>
					    <tr>
					      <td  class="cancellation"scope="row">Less than 14 days before the “Troubleshooting with Wireshark” start date</th>
					      <td>No Refund</td>
					    </tr>
					  </tbody>
					</table>
					<p>If unable to attend the scheduled training class, please call Wireshark University at (408)378-7841 to cancel your registration. If you do not show up for a scheduled course without prior notification (“no show”), no refund will be given. Student substitutions are allowed, but notification must be made to <a href="mailto:info@wiresharktraining.com"> info@wiresharktraining.com</a> no less than five (5) full business days before the start of the class (not including the class start date).</p>

			      </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>
</div>


<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>