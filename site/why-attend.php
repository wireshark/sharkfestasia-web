<?php include "header-small.php"; ?>
<div class="col-lg-12 agenda-header why-attend">
	<div class="col-lg-5 agenda-header why-image">
		
	</div>
	<div class="col-lg-7 agenda-header-background">
		<h3>
			<p></p>
			<a target="_blank" href="/docs/SharkFest18ASIAAgenda.pdf">
				<button type="button" class="btn btn-primary btn-xl">Full Conference Session Agenda</button>
			</a>
		</h3>
		
	</div>
</div>
<div  id="about" class="col-lg-12 why-tabs-background">
	<div class="col-centered why-list-div">
		<!--<ul class="why-list col-centered">
			<li><a href="#about">About SharkFest Europe</a></li>
			<li><a href="#location">Location</li></a>
			<li><a href="#why">Why Attend</li></a>
			<li><a href="#faq">FAQ</li></a>
		</ul>-->
	</div>
</div>


<!--<div class="container-fluid why-attend">
	<div class="col-lg-9 why-body">
		<h2 >About SharkFest Europe</h2>
			<p>SharkFest is an annual educational conference focused on sharing knowledge, experience and best practices among members of the Wireshark global developer and user communities. Participants come together to hone their skills in the art of packet analysis by attending a rich and varied schedule of sessions taught by the most seasoned experts in the industry.</p>
		<h2>About Palácio Estoril Hotel</h2>
            <p>Built in 1930, today’s Palácio Hotel retains many of the characteristics of that period. The ambience of exclusiveness that pervades the Hotel makes an impression on everybody who enters it, from its imposing, entirely white facade and beautiful gardens to its elegant classic decor, which has been updated over the years but without losing its combination of timeless, luxury and sophistication.</p>
            <p>During the second World War, due to Portugal's neutrality, several royal families went into exile in Estoril, which became known as the "Coast of Kings".  The Hotel Palácio was the chosen home of numerous members of European royalty and was also the haunt of British and German spies, who could often be found in its bar. Later, these stories of intrigue and espionage inspired famous novelists and filmmakers and the Hotel served as the set for the James Bond movie <i>On Her Majesty's Secret Service.</i></p>

            <p id="location">The Palácio Estoril was the second home of the Spanish, Italian, French, Bulgarian and Romanian royal families, and even today remains a favourite with their descendants. In February 2011, the Royal Gallery was created in their honour, where you can view photographs of the important members of European royalty who stayed at the Hotel. The Royal Gallery is now one of Estoril's tourist attractions.
            
        <h2>Location</h2>
        	<p><strong>Palacio Esoril Hotel</strong><br>
        	R. Particular à Avenida Biarritz, 2769 504 Estoril, Portugal<br>
        	<a href="http://www.palacioestorilhotel.com/" target="_blank">http://www.palacioestorilhotel.com/</a>
        	</p>

		</div>
	<div class="col-lg-3 col-md-12 about-gallery">
		<!--<a style="font-size: 25px;" data-lightbox="sf16eu" alt="Click Here to View Pictures from Sharkfest '16 Europe!" href='img/sharkfest16eugallery/2.jpg'><img class="img-responsive" src="img/sharkfest16eugallery/sfeugal.jpg" ></a>
        <img id="why"  class="img-responsive" src="img/estoril-vintage.jpg">
    </div>
                    <a href='img/sharkfest16eugallery/1.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/3.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/4.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/5.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/6.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/7.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/8.jpg' data-lightbox="sf16eu"></a>
                    
                    <a href='img/sharkfest16eugallery/9.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/10.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/11.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/12.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/13.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/14.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/15.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/16.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/17.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/18.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/19.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/20.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/21.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/22.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/23.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/24.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/25.jpg' data-lightbox="sf16eu"></a>
</div> -->
<div class="container-fluid why-attend why-body">
	<div class="col-lg-9">
		<h2>Why Attend?</h2>
		<!--<p>Rerum delectus rerum enim autem odio velit id. Eos velit facere consectetur dolor. Qui sit eligendi voluptatem consequuntur et facilis. Deserunt omnis accusantium quas magni neque accusamus.</p>-->
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>1</h2>
			</div>
			<div class="why-reason-text">
				<p><strong>Thrive</strong> in a unique, immersive Wireshark educational and social networking experience.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>2</h2>
			</div>
			<div class="why-reason-text">
				<p><strong> Build diagnostic muscle </strong>by attending a selection of 20 Wireshark-centric sessions ranging from beginner to advanced/developer levels. </p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>3</h2>
			</div>
			<div class="why-reason-text">
				<p><strong>World-class Wireshark instruction</strong> by renowned veteran packet analysts.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>4</h2>
			</div>
			<div class="why-reason-text">
				<p><strong> Meet the devs. </strong> Gerald Combs, founder of Ethereal/Wireshark open source project, and reps from the global network of core developers will be on hand to answer questions + present the Wireshark projects current and future direction.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>5</h2>
			</div>
			<div class="why-reason-text">
				<p><strong>Laura Chappell!</strong>  Jettison your analytical skills to untold heights by attending a 1-day pre-conference troubleshooting class with one of the planet’s foremost Wireshark trainers.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>6</h2>
			</div>
			<div class="why-reason-text">
				<p><strong>  Test your investigative chops</strong> with fellow attendees at the PacketPalooza esPcape group packet competition.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>7</h2>
			</div>
			<div class="why-reason-text">
				<p><strong> Discover Wireshark use-enhancing technology</strong> from SharkFest sponsors during technology showcase hours.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>8</h2>
			</div>
			<div class="why-reason-text">
				<p><strong> Build your professional network</strong> in a casual, social setting.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>9</h2>
			</div>
			<div class="why-reason-text">
				<p><strong>   Fun.</strong> Revel with attendees at evening social events and on the beautiful Nanyang Technological University campus! </p>
			</div>
		</div>
	</div>
	<div class="col-lg-3" style="display: flow-root;">
		<h2 class="orange-h2">New to SharkFest?</h2>
		<a href="first-time"><button><strong>Click here to go to our New to SharkFest page</strong></button></a>
	</div>
</div>
<!--<div class="container-fluid why-body">
	<div class="col-lg-12">
		<h2 id="faq">FAQ</h2>
		<div id="accordion" role="tablist" aria-multiselectable="true">
		  <div class="card">
		    <div class="card-header" role="tab" id="headingOne">
		      <h5 class="mb-0">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		          Example
		        </a>
		      </h5>
		    </div>

		    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
		      <div class="card-block">
		        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
		      </div>
		    </div>
		  </div>
		  <div class="card">
		    <div class="card-header" role="tab" id="headingTwo">
		      <h5 class="mb-0">
		        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
		          Example
		        </a>
		      </h5>
		    </div>
		    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
		      <div class="card-block">
		        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
		      </div>
		    </div>
		  </div>
		  <div class="card">
		    <div class="card-header" role="tab" id="headingThree">
		      <h5 class="mb-0">
		        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
		          Example
		        </a>
		      </h5>
		    </div>
		    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
		      <div class="card-block">
		        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</div>-->


<?php include  "footer.php"; ?>