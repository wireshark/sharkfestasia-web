<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

    <!-- Content area -->
    <div class="content-area retrospective-page post-excerpt">

        <!-- PAGE BLOG -->
        <section class="page-section with-sidebar sidebar-right">
        <div class="container">
        <div class="row">

        <!-- Content -->
        <section id="content" class="content col-sm-8 col-md-9">

            <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                <div class="post-media">
                    
                    <img src="img/retrospective/sf18asia.jpg" alt=""/>
                </div>
                <div class="post-header">
                    <h2 class="post-title"><a href="#">SharkFest'18 ASIA Retrospective</a></h2>
                    <div class="post-meta">
                        
                        
                    </div>
                </div>
                <div class="post-body">
                    <div class="post-excerpt">

                        <p>April 9th - 11th, 2018 <br>
                        Nanyang Technological University, Singapore</p>
                        <!--<h2 class="post-title2"><strong>SharkFest’18 ASIA Attendee Feedback</strong></h2>
                            <blockquote>“It’s a wonderful time to have the privilege to participate with such talent
                            and good will as that which is found in the Wireshark community. I believe
                            that as the risks present within the Internet increase, Wireshark and
                            the Wireshark community stand poised at the intersection of the Internet
                            nexus to identify, mitigate, and secure network resources everywhere.<br><br>
                             
                            Thank you again for all that you do to bring together the Wireshark
                            community annually for SharkFest!”</blockquote>
                            <blockquote>“Thank you for organizing a fabulous event for everyone. It was a great experience and I learned a lot.”</blockquote>
                            <blockquote>
                            “In my humble opinion, Sharkfest is a great success. Many of the presentations show common case issues, and how Wireshark helps to identify them in their own ways. Most of the people I met at Sharkfest are small to medium size companies. We all have similar issues, but sometimes we just don’t know how and where to begin tackling the issue. It looks so easy when the experts present, trust me it’s not quite similar in real life. This conference helps us to navigate through these obstacle courses and helps us to do our job better.”</blockquote>
                            <blockquote>“Thank you for the great SharkFest' 18 US conference in Pittsburgh, Pennsylvania for the past days. All of you – and don’t forget the guys behind the scenes - have done a great job again and we all together survived.”</blockquote>
                        <!--
                        <h2 class="post-title2"><strong>Blogs</strong></h2>
                        <p><a href="http://blog.packet-foo.com/2014/06/sharkfest-2014-recap/">SharkFest 2015 Recap</a> by Jasper Bongertz</p> -->
                        <!--
                        <h2 class="post-title2"><strong><a href="assets/presentations15/packetchallenge.pdf">Packet Challenge</a></strong></h2>
                        <p>Click <a href="assets/presentations15/packetchallenge.zip">HERE</a> to download the Packet Challenge questions/answers and the pcap files!  
                        </p> -->
                            
                        <h2 class="post-title2"><strong>Keynote Presentation</strong></h2>
                         <div class="responsive-iframe">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/IZ0kmvQL5JM" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <p><strong>Wireshark: Past, Present & Future</strong><br/>
                        Gerald Combs<br><br>
                        Video coming soon!</p>

                        <!--<div class="responsive-iframe">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/WyVgEof2uKg" frameborder="0" allowfullscreen></iframe>
                        </div>  -->
                         
                        
                        <h2 class="post-title2"><strong>Tuesday Classes</strong></h2>
                        <ul style="list-style:none;">
                        <li>01: <a href="#">In the Packet Trenches - Part 1</a> by Hansang Bae</li>
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/d62QJlSqlgs" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:12:42)</li>
                        </ul>-->
                        <li>02:<a href="assets/presentations18/02.7z">  Writing a Wireshark Dissector: 3 Ways to Eat Bytes</a> by Graham Bloice</li>
                        
                        <li>03: <a href=""> In the Packet Trenches - Part 2</a> by Hansang Bae</li>
                        <!--<ul>
                        <li><a href="assets/presentations18/grahambloice.zip"> Presentation Materials</a></li>
                        </ul>-->
                        <ul>
                        </ul>
                        <li>04: <a href="assets/presentations18/04.pdf"> Wireshark Saves the Day! A Beginner’s Guide to Packet Analysis</a> by Maher Adib</li>
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/UOvs0e5df8U" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:14:11)</li>
                        </ul>-->
          
                        <li>05: <a href="assets/presentations18/05.pdf"> Sneaking in by the Back Door: Hacking the non-standard layers with Wireshark - Part 1</a> by Phill Shade</li>
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/15wDU3Wx1h0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:02:21)</li>
                        </ul>-->
                        <li>06: <a href="assets/presentations18/06.pptx"> Developer Bytes Lightning Talks</a> by Wireshark Core Developers</li>
                        <ul>
                            <li class="presVideo"><a href="https://youtu.be/gBMMGHVfMGE" target="_blank">Presentation Video</a> (1:07:00)</li>
                        </ul>
                        <li>07: <a href="#"> Sneaking in by the Back Door: Hacking the non-standard layers with Wireshark - Part 2</a> by Phill Shade</li>
                        
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/o53DFI_Y3QQ"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:14:06)</li>
                        </ul> -->
                        <li>08: <a href="assets/presentations18/08.zip">  TCP SACK Overview and Impact on Performance</a> by John Pittle</li>
                        
                        <li>09: <a href="assets/presentations18/09.pptx"> Using Wireshark to Solve Real Problems for Real People: Step-by-Step Case Studies in Packet Analysis </a>by Kary Rogers</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/TLjEYPFZCf8" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:10:38)</li>
                        </ul> 
                        
                        <li>10: <a href="#"> Augmenting Packet Capture with Contextual Meta-Data: the what, why and how </a>by Dr. Stephen Donnelly</li>
                       
                         <ul>
                        <li class="presVideo"><a href="https://youtu.be/WcVS6AZyDng" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (57:06)</li>
                        </ul> 
                        

                        </ul>

                        <h2 class="post-title2"><strong>Wednesday Classes</strong></h2>
                        <ul style="list-style:none;">
                        <li>11: <a href="assets/presentations18/11.pdf">Wireshark CLI Tools and Scripting</a>by Sake Blok</li>
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/HTtVHxIh6ww" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:19:03)</li>
                        </ul>-->

                        <li>12: <a href="assets/presentations18/12.pdf">Filter Maniacs: Tips & Techniques for Wireshark display & winpcap/libpcap capture filters</a> by Megumi Takeshita</li>
                        <li> 13:<a href="assets/presentations18/13.zip"> Designing a Packet Capture Strategy...and how it fits into an overall performance visibility strategy</a> by John Pittle</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/sKO0IVgjOTk" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:42)</li>
                        </ul>
                        <li>14: <a href="assets/presentations18/14.pdf"> Wireshark and Layer 2 Control Protocols</a> by Werner Fischer</li>
                        <li>15: <a href="#">LTE Explained... The Other Protocols</a> by Mark Stout</li> 
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/zPZgeCRyz9c" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (58:21)</li>
                        </ul> 
                        <li>16: <a href="assets/presentations18/16.pptx"> extcap – Packet Capture beyond libpcap/winpcap: Bluetooth Sniffing, Android Dumping & other Fun Stuff! </a> by Roland Knall</li> 
                         <ul>
                        <li class="presVideo"><a href="https://youtu.be/6opeMOl7uJo" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (46:26)</li>
                        </ul> 
                        <li>17: <a href="#"> The Packet Doctors are In! </a>by Drs: Hansang Bae, Phill Shade, Sake Blok</li>
                        
                       
                        <li>18: <a href="assets/presentations18/18.pptx"> Understanding Throughput & TCP Windows: factors that can limit TCP throughput performance</a> by Kary Rogers</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/ZxSi4M941Bs" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (59:40)</li>
                        </ul>

                        <li>19: <a href="assets/presentations18/19.pdf" title="Presentation slides" target="_blank">SSL/TLS Decryption: Uncovering Secrets </a> by Peter Wu</li>
                         <ul>
                        <li class="presVideo"><a href="https://youtu.be/bwJEBwgoeBg" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:06:28)</li>
                        </ul> 
                        <li>20: <a href="assets/presentations18/20.pdf" title="Presentation slides">How Did They Do That? Network Forensic Case Studies </a> by Phill Shade</li>                       

                      </ul>

                  
                       
                    </div>
                </div>
                
            </article>

            <!-- About the author -->
            
            <!-- /About the author -->

            <!-- Comments -->
            
            <!-- /Comments -->

            <!-- Leave a Comment -->
            
            <!-- /Leave a Comment -->

        </section>
        <!-- Content -->

        <hr class="page-divider transparent visible-xs"/>

        <!-- Sidebar -->
        <aside id="sidebar" class="sidebar col-sm-4 col-md-3">
        <h4 class="widget-title">A Word of Thanks</h4>
        <p>SharkFest&#39;18 ASIA, on the 20th anniversity of the Wireshark Project, and the first ever in the Asia Pacific was a roaring success thanks to the highly engaged community of core developers and Wireshark users in attendance. Special thanks to Gerald Combs for tirelessly, fearlessly guiding the Wireshark open source project and maintaining its relevancy, to core developers for traveling long distances to advance Wireshark code together, to Laura Chappell for creating another highly-anticipated Packet Challenge, to Sake Blok for the many man-hours dedicated to creating a thrilling group packet competition, to a staff and volunteer crew who went far beyond caring to serve attendees during the conference, to instructors who voluntarily shuffled lives and schedules to educate participants and learn from one another, to sponsors who so generously provided resources that made the conference possible, to the NTU social hosts who made our social events truly social, and to the team for working through months of minutiae to help stage the conference on the lush and beautiful NTU campus.</p>
            <div class="widget categories">
                <!--
                <h4 class="widget-title">Past Years</h4>
                <ul>
                    <li><a href="sf14.html">SharkFest'14</a></li>
                    <li><a href="sf13.html">SharkFest'13</a></li>
                    <li><a href="sf12.html">SharkFest'12</a></li>
                    <li><a href="sf11.html">Sharkfest'11</a></li>
                    <li><a href="sf10.html">Sharkfest'10</a></li>
                    <li><a href="sf09.html">Sharkfest'09</a></li>
                    <li><a href="sf08.html">Sharkfest'08</a></li>
                </ul> -->
            </div>
            
            <div class="widget flickr-feed">
                
               

        
        <div class="post-media">
        <a style="font-size: 25px;" data-lightbox="sf18" alt="Click Here to View Pictures from Sharkfest'18 ASIA!" href='img/sharkfest18gallery/1.jpg'><img src="img/sharkfest18gallery/sf18gal.jpg" ></a></div>
            <a href='img/sharkfest18gallery/2.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/3.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/4.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/5.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/6.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/7.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/8.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/9.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/10.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/11.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/12.jpg' data-lightbox="sf18"></a>
            
            <a href='img/sharkfest18gallery/13.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/14.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/15.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/16.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/17.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/18.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/19.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/20.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/21.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/22.jpg' data-lightbox="sf18"></a>
            
            <a href='img/sharkfest18gallery/23.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/24.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/25.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/26.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/27.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/28.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/29.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/30.jpg' data-lightbox="sf18"></a>
            <!--<a href='img/sharkfest18gallery/25.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/25-5.jpg' data-lightbox="sf18"></a>-->

           

            </div>
             <script>
            jQuery('a.gallery').colorbox();
        </script> 

                
        


<!--
            <div class="widget tag-cloud">
                <h4 class="widget-title">Tags</h4>
                <ul>
                    <li><a href="#">Creative</a></li>
                    <li><a href="#">Design</a></li>
                    <li><a href="#">Art</a></li>
                    <li><a href="#">Autpor Name</a></li>
                    <li><a href="#">Coorperate</a></li>
                    <li><a href="#">Wordpress</a></li>
                    <li><a href="#">3D Animations</a></li>
                </ul>
            </div>

            <div class="widget">
                <h4 class="widget-title">Join The Newsletter</h4>
                <form method="post" action="#">
                    <div class="form-group">
                        <input type="text" placeholder="Type and hit enter... " class="form-control" name="search" title="search">
                        <input type="submit" hidden="hidden"/>
                    </div>
                </form>
            </div>

        </aside>
        <!-- Sidebar -->

    <div class="sponsor-list">
        <h4 style="margin-top:40px; margin-bottom:5px" class="widget-title">Sponsors</h4>
                <p><strong>Host Sponsors</strong></p>
                <a href="http://www.riverbed.com/" target="_blank"><img class="sponsors" style="margin-top: -10px; margin-bottom: 20px;" src="img/sponsors/riverbed.png"></a>
                <a href="http://www.wiresharktraining.com/" target="_blank"><img style="width:130px; margin-bottom: 20px; margin-top: 10px" class="sponsors" src="img/sponsors/wireshark_university_logo.png"></a>

                <p style="margin-bottom: 15px"><strong>Angel Shark Sponsors</strong></p>
                <a target="_blank" href="http://www.profitap.com/"><img style="width:125px; margin-bottom: 10px;" class="sponsors" src="img/sponsors/profitap.png"></a>            

                <p><strong>Group Packet Challenge Sponsor</strong></p>
                
                <a target="_blank" href="https://www.endace.com/"><img class="sponsors" style="width: 140px; margin-bottom:20px; margin-top: -5px" src="img/sponsors/endace_big.png"></a>
            
                <p><strong>Honorary Sponsors</strong></p>
                <a href="http://www.lovemytool.com/" target="_blank"><img style="width:125px;" class="sponsors" src="img/sponsors/lovemtytool_Logo.png"></a>
                                    
        </div>
        </div>
        </section>
        </aside>
        <!-- /PAGE BLOG -->

    </div>
    <!-- /Content area -->

    <?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>



