<?php
 


if(isset($_POST['email'])) {
 
     
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
 
    $email_to = "spampinatodesign@gmail.com, sharkfest@riverbed.com, laura@chappellu.com";
 
    $email_subject = "SharkFest'18 ASIA Speaker Form Submission";
 
     
 
     
 
    function died($error) {
 
        // your error code can go here
 
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
 
        echo "These errors appear below.<br /><br />";
 
        echo $error."<br /><br />";
 
        echo "Please go back and fix these errors.<br /><br />";
 
        die();
 
    }
 

 
    // validation expected data exists
 
    if(!isset($_POST['fName']) ||
 
        !isset($_POST['lName']) ||
 
        !isset($_POST['email']) ||
 
        !isset($_POST['cell']) ||
 
        !isset($_POST['company']) ||

        !isset($_POST['title']) ||

        !isset($_POST['address']) ||

        !isset($_POST['address2']) ||

        !isset($_POST['city']) ||

        !isset($_POST['state']) ||

        !isset($_POST['zip']) ||

        !isset($_POST['shirt']) ||


        !isset($_POST['bio']) ||

        !isset($_POST['preTitle']) ||

        !isset($_POST['subTitle']) ||

        !isset($_POST['format']) ||

        !isset($_POST['abstract']) ||

        !isset($_POST['audience']) ||

        !isset($_POST['additional']))

    {
 
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
 
    }
 

 
    $fName =  ($_POST['fName']); // required
 
    $lName =  ($_POST['lName']); // required
 
    $email =  ($_POST['email']); // required
 
    $cell =  ($_POST['cell']); // required
 
    $company =  ($_POST['company']); // required

    $title =  ($_POST['title']); // required

    $address =  ($_POST['address']); // required

    $address2 =  ($_POST['address2']); // not required

    $city =  ($_POST['city']); // required

    $state =  ($_POST['state']); // required

    $zip =  ($_POST['zip']); // required

    $shirt =  ($_POST['shirt']); // required

    

    $bio =  ($_POST['bio']); // required

    $preTitle =  ($_POST['preTitle']); // required

    $subTitle =  ($_POST['subTitle']); // not required

    $format =  ($_POST['format']); // required

    $abstract =  ($_POST['abstract']); // required

    $audience =  ($_POST['audience']); // required

    $additional =  ($_POST['additional']); // not required
 
     
 
    $error_message = "";
 
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  

    $string_exp = "/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/";
  
  if(preg_match($string_exp,$cell)) {
 
    $error_message .= 'The cell phone number you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$company)) {
 
    $error_message .= 'The company you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$title)) {
 
    $error_message .= 'The job title you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$address)) {
 
    $error_message .= 'The address you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$city)) {
 
    $error_message .= 'The city you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$state)) {
 
    $error_message .= 'The state/province you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$zip)) {
 
    $error_message .= 'The postal code you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$shirt)) {
 
    $error_message .= 'The shirt size you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$bio)) {
 
    $error_message .= 'Please enter your biography.<br />';
 
  }

   if(preg_match($string_exp,$preTitle)) {
 
    $error_message .= 'Please enter the title of your presentation.<br />';
 
  }

   if(preg_match($string_exp,$format)) {
 
    $error_message .= 'Please enter the format of your presentation.<br />';
 
  }

   if(preg_match($string_exp,$abstract)) {
 
    $error_message .= 'Please enter the abstract.<br />';
 
  }

   if(preg_match($string_exp,$audience)) {
 
    $error_message .= 'Please enter the audience level of your presentation.<br />';
 
  }

  if(strlen($error_message) > 0) {
 
    died($error_message);
 
  }
 
    $email_message = "";
 
     
 
    function clean_string($string) {
 
      $bad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($bad,"",$string);
 
    }


     
 
    $email_message .= "First Name: ".clean_string($fName)."\n";
 
    $email_message .= "Last Name: ".clean_string($lName)."\n";
 
    $email_message .= "Email: ".clean_string($email)."\n";
 
    $email_message .= "Cell Phone: ".clean_string($cell)."\n";
 
    $email_message .= "Company: ".clean_string($company)."\n";

    $email_message .= "Title: ".clean_string($title)."\n";

    $email_message .= "Address: ".clean_string($address)."\n";

    $email_message .= "Address2: ".clean_string($address2)."\n";

    $email_message .= "City: ".clean_string($city)."\n";

    $email_message .= "State: ".clean_string($state)."\n";

    $email_message .= "Zip: ".clean_string($zip)."\n";

    $email_message .= "Shirt Size: ".clean_string($shirt)."\n";

    $email_message .= "Bio: ".clean_string($bio)."\n";

    $email_message .= "Presentation Title: ".clean_string($preTitle)."\n";

    $email_message .= "Subtitle: ".clean_string($subTitle)."\n";

    $email_message .= "Format: ".clean_string($format)."\n";

    $email_message .= "Abstract: ".clean_string($abstract)."\n";

    $email_message .= "Audience Level: ".clean_string($audience)."\n";

    $email_message .= "Additional Information: ".clean_string($additional)."\n";
     
 
     
 
// create email headers

$email_from = $headers;
 
$headers = 'From: sharkfest-do-not-reply@wireshark.org'.$email_from."\r\n".
 
'Reply-To: '.$email_from."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
@mail($email_to, $email_subject, $email_message, $headers);  
 
}


?>



 
 
<!-- include your own success html here -->
 
 
 <!DOCTYPE html>
<html class="full" lang="en">
<!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SharkFest™</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/full.css" rel="stylesheet">

    <!-- Oswald Font -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">

    <link href="css/lightbox.css" rel="stylesheet" type="text/css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon'/ >
</head>    <!-- /HEADER -->

    <!-- Content area -->
    <div class="content-area">

        <!-- PAGE BLOG -->
        <section class="page-section with-sidebar sidebar-right">
        <div class="container">
        <div class="row">

        <!-- Content -->
        <section id="content" class="content col-sm-8 col-md-9">

            <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                <!--<div class="post-media">
                    
                    <img src="assets/img/sf08.jpg" alt=""/>
                </div>-->
                <div class="post-header" style="">
                    <h2 class="post-title">Thank you!</h2>
                    <div class="post-meta">
                        
                        
                    </div>
                </div>
                <div class="post-body">
                    <div class="post-excerpt">

                        <p>Thank you for filling out the SharkFest speaker application form! We will be in touch with you soon.</p>

                        
                    </div>
                </div>
                
            </article> 
        </section>
        <!-- Content -->
        <hr class="page-divider transparent visible-xs"/>

        <!-- Sidebar -->
        <aside id="sidebar" class="sidebar col-sm-4 col-md-3">
        
            

        </div>
        </div>
        </section>
        <!-- /PAGE BLOG -->

    </div>
    <!-- /Content area -->

    

   

</div>


 
 
 
<?php


include_once 'connection.php';

$conn    = Connect();


$query = "INSERT into sf18asiaspeakers (fName, lName, email, cell, company, title, address, address2, city, state, zip, shirt, bio, preTitle, subTitle, format, abstract, audience, additional) VALUES ('" . $fName . "', '" . $lName . "', '" . $email . "', '" . $cell . "', '" . $company . "', '" . $title . "', '" . $address . "', '" . $address2 . "', '" . $city . "', '" . $state . "', '" . $zip . "', '" . $shirt . "', '" . $bio . "', '" . $preTitle . "', '" . $subTitle . "', '" . $format . "', '" . $abstract . "', '" . $audience . "', '" . $additional . "')";

$success = $conn->query($query);
        if (!$success) {
            die("Couldn't enter data: ".$conn->error);}
    $conn->close();

include $_SERVER['DOCUMENT_ROOT']."/footer-absolute.php";
?>
