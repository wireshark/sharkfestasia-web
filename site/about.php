<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container">
	<div class="row">
		<div class="col-lg-8 about-text">
			<h2>What is SharkFest?</h2>
			<p><a href="https://sharkfest.wireshark.org">SharkFest™</a>, launched in 2008, is a series of annual educational conferences staged in various parts of the globe and focused on sharing knowledge, experience and best practices among the <a href="https://wireshark.org">Wireshark®</a> developer and user communities.<p>
 
                  <p>SharkFest attendees hone their skills in the art of packet analysis by attending lecture and lab-based sessions delivered by the most seasoned experts in the industry. Wireshark core code contributors also gather during the conference days to enrich and evolve the tool to maintain its relevance in ensuring the productivity of modern networks.</p>
			<h2>What is Wireshark?</h2>
                  <p>Wireshark is the de facto standard and most widely-deployed open-source network and packet analysis tool consistently downloaded 1M+ times/month by IT Professionals across all network-related disciplines.  <a href="https://riverbed.com">Riverbed</a> is the current host and corporate sponsor of the Wireshark project, Wireshark Foundation and SharkFest. </p>
			<h2>SharkFest Mission</h2>
			<p>SharkFest’s aim is to support ongoing Wireshark development, to educate and inspire current and future generations of computer science and IT professionals responsible for managing, troubleshooting, diagnosing and securing legacy and modern networks, and to encourage widespread use of the free analysis tool.  Per Gerald Combs, Wireshark project Founder …“Wireshark is a tool and a community.  My job is to support both”.</p>
                  <h2>SharkFest GOALS</h2>
			<ol>
                        <li>To educate current and future generations of network engineers, network architects, application engineers, network consultants, and other IT professionals in best practices for troubleshooting, securing, analyzing, and maintaining productive, efficient networking infrastructures through use of the Wireshark free, open source analysis tool.</li>
                        <li>To share use cases and knowledge among members of the Wireshark user and developer communities in a relaxed, informal milieu.</li>
                        <li>To remain a self-funded, independent, educational conference hosted by a corporate sponsor.</li>
                  </ol>
                  <h3>Click <a href="assets/directions.pdf"> HERE</a> for directions on how to get to Nanyang Executive Centre</h3>
		</div>
		<div class="col-lg-4 col-md-12 about-gallery">
			<a style="font-size: 25px;" data-lightbox="sf18" alt="Click Here to View Pictures from Sharkfest'18 ASIA!" href='img/sharkfest18gallery/1.jpg'><img src="img/sharkfest18gallery/sf18gal.jpg" ></a></div>
            <a href='img/sharkfest18gallery/2.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/3.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/4.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/5.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/6.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/7.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/8.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/9.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/10.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/11.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/12.jpg' data-lightbox="sf18"></a>
            
            <a href='img/sharkfest18gallery/13.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/14.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/15.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/16.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/17.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/18.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/19.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/20.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/21.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/22.jpg' data-lightbox="sf18"></a>
            
            <a href='img/sharkfest18gallery/23.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/24.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/25.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/26.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/27.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/28.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/29.jpg' data-lightbox="sf18"></a>
            <a href='img/sharkfest18gallery/30.jpg' data-lightbox="sf18"></a>
		</div> 
	</div>
</div>


<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>
