<?php include($_SERVER[DOCUMENT_ROOT] . "/header-small.php"); ?>
<div class="col-lg-12 agenda-header">
    <div class="col-lg-5 agenda-header agenda-image">
        
    </div>
    <div class="col-lg-7 agenda-header-background">
        <h2>SharkFest'18 ASIA Agenda-in-Brief


            <p>If you have any questions, please contact us: <a href="mailto:sharkfest@wireshark.org"> sharkfest@wireshark.org</a></p>
            <a target="_blank" href="/docs/SharkFest18ASIAAgenda.pdf">
                <button type="button" class="btn btn-primary btn-xl">Full Agenda with Bios & Abstracts</button>
            </a>
        </h2>
        
    </div>
</div>

<div class="container-fluid">
    <div class="row schedule-lvl1">
        <ul class="nav nav-pills nav-justified schedule">
            <li class="active"><a data-toggle="pill" href="#tab-first"><h4>Day 01<br><span>4.9.2018</span></h4></a></li>
            <li><a data-toggle="pill" href="#tab-second"><h4>Day 02<br><span>4.10.2018</span></h4></a></li>
            <li><a data-toggle="pill" href="#tab-third"><h4>Day 03<br><span>4.11.2018</span></h4></a></li>
            
         </ul>
    </div>
    <div class="tab-content">
        <div id="tab-first" class="table-responsive container tab-pane fade in active">
            <table class="schedule-table table">
                <tbody>
                    <tr>
                        <th>7:30am - 9:00am</th>
                        <th><h4>Laura Chappell’s Pre-Conference Troubleshooting Course Check-in and Badge Pick up</h4></th>
                    </tr>
                    <tr>
                        <th>7:30am - 9:00am</th>
                        <th><h4>Breakfast</h4></th>
                    </tr>
                    <tr>
                        <th>9:00am - 12:00pm</th>
                        <th><h4>Laura Chappell’s Pre-Conference Troubleshooting Course</h4></th>
                    </tr>
                    <tr>
                        <th>12:00pm - 1:00pm</th>
                        <th><h4>Lunch</h4></th>
                    </tr>
                    <tr>
                        <th>1:00pm - 5:00pm</th>
                        <th><h4>Laura Chappell’s Pre-Conference Troubleshooting Course</h4></th>
                    </tr>
                    <tr>
                        <th>12:00pm - 8:30pm</th>
                        <th><h4>Check-In & Badge Pick-Up for SharkFest’18 ASIA</h4></th>
                    </tr>
                    <tr>
                        <th>1:00pm - 5:00pm</th>
                        <th><h4>Developer Drop-In Workshop<br>
                                (SharkFest’18 ASIA Attendees Only)</h4></th>
                    </tr>
                    <tr>
                        <th>6:00pm - 8:00pm</th>
                        <th><h4>Welcome Dinner & Sponsor Showcase Reception <br> (SharkFest'18 ASIA Attendees Only)</h4></th>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="tab-second" class="tab-pane fade">
            <div class="tab-content lvl2">
                <ul class="nav nav-pills nav-justified schedule">
                    <li class="active"><a data-toggle="pill" href="#lvl21-tab-first"><h4>Lecture Room 1</h4></a></li>
                    <li><a data-toggle="pill" href="#lvl21-tab-second"><h4>Lecture Room 2</h4></a></li>
                    <!--<li><a data-toggle="pill" href="#lvl21-tab-third"><h4>McKenna/Peter/Wright</h4></a></li>-->
                 </ul>
                 <div id="lvl21-tab-first" class="table-responsive container tab-pane fade in active">
                    <table class="schedule-table table">
                        <tbody>
                            <tr>
                                <th>7:30am - 8:30am</th>
                                <th><h4>Breakfast</h4></th>
                            </tr>
                            <tr>
                                <th>7:30am - 12:00pm</th>
                                <th><h4>SharkFest Check-in</h4></th>
                            </tr>
                            <tr>
                                <th>8:30am - 9:30am</th>
                                <th><h4>Keynote: “Wireshark: Past, Present & Future” - <a href="bios/gerald-combs">Gerald Combs</a> & Friends <br>(Auditorium)</h4></th>
                            </tr>
                            <tr>
                                <th>9:45am - 11:00am</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>01: In the Packet Trenches (Part 1)</h4>
                                    <p>Instructor: <a href="bios/hansang-bae">Hansang Bae</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>11:15am - 12:30pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>03: In the Packet Trenches (Part 2)</h4>
                                    <p>Instructor: <a href="bios/hansang-bae">Hansang Bae</a></p>
                                </th>            
                            </tr>
                            <tr>
                                <th>1:30pm - 2:45pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>05: Sneaking in by the Back Door: Hacking the non-standard layers with Wireshark (Part 1)</h4>
                                    <p>Instructor: <a href="bios/phill-shade">Phill Shade</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>3:00pm - 4:15pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>07: Sneaking in by the Back Door: Hacking the non-standard layers with Wireshark (Part 2)</h4>
                                    <p>Instructor: <a href="bios/phill-shade">Phill Shade</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>4:30pm - 5:45pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>09: Using Wireshark to Solve Real Problems for Real People: Step-by-Step Case Studies in Packet Analysis</h4>
                                    <p>Instructor: <a href="bios/kary-rogers">Kary Rogers</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>6:00pm - 8:00pm</th>
                                <th>
                                    <h4>Sponsor Technology Showcase Reception, Treasure Hunt & Dinner<br> (Atrium)</h4>
                                    
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="lvl21-tab-second" class="table-responsive container tab-pane fade">
                    <table class="schedule-table table">
                        <tbody>
                            <tr>
                                <th>9:45am - 11:00am</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>02: Writing a Wireshark Dissector: 3 Ways to Eat Bytes</h4>
                                    <p>Instructor: <a href="bios/graham-bloice">Graham Bloice</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>11:15am - 12:30pm</th>
                                <th><img src="img/littlefin.png">
                                    <h4>04: Wireshark Saves the Day! A Beginner’s Guide to Packet Analysis</h4>
                                    <p>Instructor: <a href="bios/maher-adib">Maher Adib</a></p>
                                </th>            
                            </tr>
                            <tr>
                                <th>1:30pm - 2:45pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>06: Developer Bytes Lightning Talks</h4>
                                    <p>Instructor: Wireshark Core Developers</p>
                                </th>
                            </tr>
                            <tr>
                                <th>3:00pm - 4:15pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>08: TCP SACK Overview and Impact on Performance</h4>
                                    <p>Instructor: <a href="bios/john-pittle">John Pittle</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>4:30pm - 5:45pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>10: Augmenting Packet Capture with Contextual Meta-Data: the what, why and how</h4>
                                    <p>Instructor: <a href="bios/stephen-donnelly">Dr. Stephen Donnelly</a></p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="lvl21-tab-third" class="table-responsive container tab-pane fade">
                    <table class="schedule-table table">
                        <tbody>
                            <tr>
                                <th>9:30am - 10:45am</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>03: Using Wireshark to Solve Real Problems for Real People: Step-by-Step Real-World Case Studies in Packet Analysis</h4>
                                    <p>Instructor: <a href="bios/kary-rogers">Kary Rogers</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>11:00am - 12:15pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>06: Workflow-based Analysis of Wireshark Traces: Now we can all be Experts</h4>
                                    <p>Instructor: <a href="bios/paul-offord">Paul Offord</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>1:15pm - 2:30pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>09: Designing a Requirements-Based Packet Capture Strategy</h4>
                                    <p>Instructor: <a href="bios/john-pittle">John Pittle</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>2:45pm - 4:00pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>12: Baselining with Wireshark to Identify & Stop Unwanted Communications</h4>
                                    <p>Instructor: <a href="bios/jon-ford">Jon Ford</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>4:15pm - 5:30pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>
                                15: Wireshark & Time: Accurate Handling of Timing When Capturing Frames</h4>
                                <p>Instructor: <a href="bios/werner-fischer">Werner Fischer</a></p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="tab-third" class="tab-pane fade">
            <div class="tab-content lvl2">
                <ul class="nav nav-pills nav-justified schedule">
                    <li class="active"><a data-toggle="pill" href="#lvl22-tab-first"><h4>Lecture Room 1</h4></a></li>
                    <li><a data-toggle="pill" href="#lvl22-tab-second"><h4>Lecture Room 2</h4></a></li>
                   <!-- <li><a data-toggle="pill" href="#lvl22-tab-third"><h4>McKenna/Peter/Wright</h4></a></li>-->
                 </ul>
                 <div id="lvl22-tab-first" class="table-responsive container tab-pane fade in active">
                    <table class="schedule-table table">
                        <tbody>
                            <tr>
                                <th>7:30am - 8:30am</th>
                                <th><h4>Breakfast</h4></th>
                            </tr>
                            <tr>
                                <th>8:45am - 10:00am</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>11: Wireshark CLI Tools and Scripting</h4>
                                    <p>Instructor: <a href="bios/sake-blok">Sake Blok</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>10:15am - 11:30am</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>13: Designing a Packet Capture Strategy...and how it fits into an overall performance visibility strategy</h4>
                                    <p>Instructor: <a href="bios/john-pittle">John Pittle</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>11:45am - 1:00pm</th>
                                <th><img src="img/littlefin.png">
                                    <h4>15: LTE Explained... The Other Protocols</h4>
                                    <p>Instructor: <a href="">Mark Stout</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>2:00pm - 3:15pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>17: The Packet Doctors are In!</h4>
                                    <p>Drs: <a href="bios/hansang-bae">Hansang Bae,</a> <a href="bios/sake-blok">Sake Blok,</a> <a href="bios/laura-chappell">Laura Chappell</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>3:30pm - 4:45pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>19: SSL/TLS Decryption: Uncovering Secrets</h4>
                                    <p>Instructor: <a href="bios/peter-wu">Peter Wu</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>5:00pm - 6:00pm</th>
                                <th>
                                    <h4>Packet Challenge Awards, Closing Comments</h4>
                                </th>
                            </tr>
                            <tr>
                                <th>6:00pm - 8:00pm</th>
                                <th>
                                    <h4>Packet Palooza Group Packet Competition Dinner & Sponsor Showcase <br>(Atrium)</h4>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="lvl22-tab-second" class="table-responsive container tab-pane fade">
                    <table class="schedule-table table">
                        <tbody>
                            <tr>
                                <th>8:45am - 10:00am</th>
                                <th><img src="img/littlefin.png">
                                    <h4>12: Filter Maniacs: Tips & Techniques for Wireshark display & winpcap/libpcap capture filters</h4>
                                    <p>Instructor: <a href="bios/megumi-takeshita">Megumi Takeshita</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>10:15am - 11:30am</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>14: Wireshark and Layer 2 Control Protocols</h4>
                                    <p>Instructor: <a href="bios/werner-fischer">Werner Fischer</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>11:45am - 1:00pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>16: extcap – Packet Capture beyond libpcap/winpcap: Bluetooth Sniffing, Android Dumping & other Fun Stuff!</h4>
                                    <p>Instructor: <a href="bios/roland-knall">Roland Knall</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>2:00pm - 3:15pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>18: Understanding Throughput & TCP Windows: factors that can limit TCP throughput performance</h4>
                                    <p>Instructor: <a href="bios/kary-rogers">Kary Rogers</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>3:30pm - 4:45pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>20: How Did They Do That? Network Forensic Case Studies</h4>
                                    <p>Instructor: <a href="bios/phill-shade">Phill Shade</a></p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="lvl22-tab-third" class="table-responsive container tab-pane fade">
                    <table class="schedule-table table">
                        <tbody>
                            <tr>
                                <th>9:45am - 11:00am</th>
                                <th><img src="img/littlefin.png">
                                    <h4>12: Analyzing Exploit Kit Traffic with Wireshark</h4>
                                    <p>Instructor: <a href="bios/bradley-duncan">Bradley Duncan</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>11:15am - 12:30pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>14: Analysis Visualizations: Creating charts inside and outside of Wireshark to speed up your Analysis</h4>
                                    <p>Instructor: <a href="bios/robert-bullen">Robert Bullen</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>1:30pm - 2:45pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>16: Undoing the Network Blame Game and Getting to the Real Root Cause of Slow Application Performance</h4>
                                    <p>Instructor: <a href="bios/chris-greer">Chris Greer</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>3:00pm - 4:15pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>18: Network Forensics with Wireshark</h4>
                                    <p>Instructor: <a href="bios/laura-chappell">Laura Chappell</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>4:30pm - 5:45pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>20: Using the Python/Django Web Framework to Build a Remote Packet Capture Portal with tshark</h4>
                                    <p>Instructor: <a href="bios/kevin-burns">Kevin Burns</a></p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="tab-fourth" class="tab-pane fade">
            <div class="tab-content lvl2">
                <ul class="nav nav-pills nav-justified schedule">
                    <li class="active"><a data-toggle="pill" href="#lvl23-tab-first"><h4>McConomy Auditorium</h4></a></li>
                    <li><a data-toggle="pill" href="#lvl23-tab-second"><h4>Connan</h4></a></li>
                    <li><a data-toggle="pill" href="#lvl23-tab-third"><h4>McKenna/Peter/Wright</h4></a></li>
                 </ul>
                 <div id="lvl23-tab-first" class="table-responsive container tab-pane fade in active">
                    <table class="schedule-table table">
                        <tbody>
                            <tr>
                                <th>8:30am - 9:30am</th>
                                <th><h4>
                                SharkBytes!</h4></th>
                            </tr>
                            <tr>
                                <th>9:45am - 11:00am</th>
                                <th><h4><img src="img/littlefin.png"><img src="img/littlefin.png">
                                31: SMB/CIFS Analysis: Using Wireshark to Efficiently Analyze & Troubleshoot SMB/CIFS</h4>
                                <p>Instructor: <a href="bios/betty-dubois">Betty DuBois</a></p></th>
                            </tr>
                            <tr>
                                <th>11:15am - 12:30pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>34: How tshark saved my SDN Forensics: Hands-on tshark Usage with a Minor Python Connection</h4>
                                    <p>Instructors: <a href="bios/mike-mcalister">Mike McAlister</a> & <a href="bios/mike-mcalister">Joseph Bull</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>1:30pm - 2:45pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>37: Back to the Packet Trenches (Part 1)</h4>
                                    <p>Instructor: <a href="bios/hansang-bae">Hansang Bae</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>3:00pm - 4:15pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>40: Back to the Packet Trenches (Part 2)</h4>
                                    <p>Instructor: <a href="bios/hansang-bae">Hansang Bae</a></p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="lvl23-tab-second" class="table-responsive container tab-pane fade">
                    <table class="schedule-table table">
                        <tbody>
                            <tr>
                                <th>9:45am - 11:00am</th>
                                <th><h4><img src="img/littlefin.png"><img src="img/littlefin.png"><img src="img/littlefin.png">
                                32: Writing a Wireshark Dissector: 3 Ways to Eat Bytes</h4>
                                <p>Instructor: <a href="bios/graham-bloice">Graham Bloice</a></p></th>
                            </tr>
                            <tr>
                                <th>11:15am - 12:30pm</th>
                                <th><img src="img/littlefin.png">
                                    <h4>35: My Life as a Troubleshooter: So what did you do today, Dad?</h4>
                                    <p>Instructor: <a href="bios/graeme-bailey">Graeme Bailey</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>1:30pm - 2:45pm</th>
                                <th><img src="img/littlefin.png">
                                    <h4>38: Wireshark Tips & Tricks</h4>
                                    <p>Instructor: <a href="bios/laura-chappell">Laura Chappell</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>3:00pm - 4:15pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>41: Analyzing Exploit Kit Traffic with Wireshark</h4>
                                    <p>Instructor: <a href="bios/bradley-duncan">Bradley Duncan</a></p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="lvl23-tab-third" class="table-responsive container tab-pane fade">
                    <table class="schedule-table table">
                        <tbody>
                            <tr>
                                <th>9:45am - 11:00am</th>
                                <th><h4><img src="img/littlefin.png"><img src="img/littlefin.png">
                                33: Wireshark & Time: Accurate Handling of Timing When Capturing Frames</h4>
                                <p>Instructor: <a href="bios/werner-fischer">Werner Fischer</a></p></th>
                            </tr>
                            <tr>
                                <th>11:15am - 12:30pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>36: Validating Your Packet Capture: How to be sure you’ve captured correct & complete data for analysis</h4>
                                    <p>Instructors: <a href="bios/scott-haugdahl">Scott Haugdahl</a> & <a href="bios/mike-canney">Mike Canney</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>1:30pm - 2:45pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>39: Knowing the Unknown: How to Monitor & Troubleshoot an Unfamiliar Network</h4>
                                    <p>Instructor: <a href="bios/luca-deri">Luca Deri</a></p>
                                </th>
                            </tr>
                            <tr>
                                <th>3:00pm - 4:15pm</th>
                                <th><img src="img/littlefin.png"><img src="img/littlefin.png">
                                    <h4>42: TCP SACK Overview & Impact on Performance</h4>
                                    <p>Instructor: <a href="bios/john-pittle">John Pittle</a></p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>