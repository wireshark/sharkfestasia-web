<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>


<div class="text-centered">
    <div class="container">
    	<div class="row">
            
            <div class="col-lg-12 retrospective-page">
                <div class=" col-lg-6">
            

                    <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                        <div class="">
                            <!--<div class="post-type">
                                <i class="fa fa-video-camera"></i> 
                            </div>-->
                            <a href="sf18asia.php"><img src="img/retrospective/sf18asia.jpg" alt=""/></a>
                        </div> 
                        <div class="post-header">
                            <h2 class="post-title"><a href="sf18asia.php">SharkFest'18 ASIA Retrospective</a></h2>
                            <div class="post-meta">
                                
                                <span class="pull-right">
                                    <!--<i class="fa fa-comment"></i> <a href="#">12</a>-->
                                </span>
                            </div>
                        </div>
                        <div class="post-body">
                            <div class="">
                                <p>SharkFest’18 ASIA was the first in the Asia Pacific!  Many IT Professionals from many countries found their way to the NEC at Nanyang Technological University in Singapore to share their Wireshark wisdom and experience, and to enjoy one another's company over the course of the conference days. To view keynotes, session presentations and more, please read on.</p>
                            </div>
                        </div>
                        <div class="post-footer">
                            <btn class="btn-primary">
                                <a href="sf18asia.php" class="btn btn-theme btn-theme-transparent">Read more</a>
                            </btn>
                        </div>
                    </article>
                </div>
                
            </div>
        </div>
    </div>
</div>

<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>
