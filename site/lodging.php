<?php include($_SERVER[DOCUMENT_ROOT] . "/header-small.php"); ?>

<div class="container">
	<div class="row">
		
			<div class="col-sm-12 col-md-12">
				<!--<h2>To book your room at the Nanyang Executive Center, please complete the <a href="docs/room-res.docx">booking form </a>and<a href="docs/credit-card.pdf"> credit card authorization form</a>. Please email both completed forms to<a href="mailto:nec-rsvn@ntu.edu.sg"> nec-rsvn@ntu.edu.sg</a>, or fax the forms to +65 6794 7860.</h2>-->
			    <div class="thumbnail">
			    <a target="_blank" href="http://www.ntu.edu.sg/nec/guestrooms/Pages/Guestrooms.aspx">
			      	<img src="img/nec-room.jpg" alt="...">
			    </a>
			      <div class="caption">
			        <h4>Nanyang Executive Center Guest Rooms</h4>
			        <ol>
			        	<li>On-campus, single and double dorm rooms with private bathroom</li>
			        	<ol>
			        		<li>Deluxe Single, $160 SGD (+10% service charge & prevailing government taxes)</li>
			        		<li>Deluxe Queen /2 Twin Beds, $180 SGD (+10% service charge & prevailing government taxes)</li>
			        	</ol>	
			        	<li>Rates include complimentary breakfast and Internet access</li>
			        	<li>Complimentary local calls</li>
			        	<li>Individually-controlled air conditioning</li>
			        	<li>Coffee and tea-making facilities</li>
			        	<li>Handicapped-friendly rooms</li>

			        </ol>
			        <p class="lodging-p">To book your room at the Nanyang Executive Center, please complete the <a href="docs/room-res.docx">booking form </a>and<a href="docs/credit-card.pdf" download> credit card authorization form</a>. Please email both completed forms to<a href="mailto:nec-rsvn@ntu.edu.sg"> nec-rsvn@ntu.edu.sg</a>, or fax the forms to +65 6794 7860.</p>
			        <p class="lodging-p">Note: Please read <a href="http://www.ntu.edu.sg/nec/guestrooms/Pages/booking.aspx">Nanyang's reservation rules and regulations</a> before submitting your forms.</p>
			        <p class="lodging-p">Click <a href="assets/directions.pdf"> HERE</a> for directions on how to get to Nanyang Executive Centre</p>
			        <p><a href="http://www.ntu.edu.sg/nec/guestrooms/Pages/Guestrooms.aspx" role="button">Learn More</a></p>
			      </div>
			    </div>
			</div>
			
			
	</div>
</div>


<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>