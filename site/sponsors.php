<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>



<div class="text-centered">
    <div class="container">
    	<div class="row sponsors-page">
            
                <h3 style="margin-bottom: 35px;">View the SharkFest’18 ASIA <a href="docs/sponsor-doc.pdf">Sponsorship Form</a> to read about the different sponsorship levels available.<br><br>If your company would like to sponsor SharkFest, please contact <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org</a>.
                </h3>
                
                <h1>SharkFest'18 ASIA Sponsors</h1>
                <h2><u>Host Sponsors</u></h2>
                <div class="col-lg-12"> 
                    <a target="_blank" href="https://www.riverbed.com">
                        <img src="img/sponsors/riverbed.png">
                    </a>
                    <p><strong>Riverbed®</strong> is the leader in Application Performance Infrastructure, delivering the most complete platform for Location-Independent Computing.™ Location-Independent Computing turns location and distance into a competitive advantage by allowing IT to have the flexibility to host applications and data in the most optimal locations while ensuring applications perform as expected, data is always available when needed, and performance issues are detected and fixed before end users notice. Riverbed's 24,000+ customers include 97% of the Fortune 100 and 95% of the Forbes Global 100. Learn more at <a href="https://www.riverbed.com">www.riverbed.com</a>.

                    </p>
                </div>
                <div class="col-lg-12">
                    <a target="_blank" href="https://www.wiresharktraining.com/">
                        <img src="img/sponsors/wiresharku.png">
                    </a>
                    <p><strong>Wireshark University</strong> delivers quality training on network analysis, security and optimization using the world's most popular open source network analyzer, Wireshark software. Wireshark University also manages the Wireshark Certified Network Analyst (WCNA) program to confirm individual competencies and knowledge of TCP/IP communications analysis, using Wireshark software to troubleshoot various network issues, identify reconnaissance processes and indications of breached hosts. Visit <a href="https://www.wiresharktraining.com/"> www.wiresharktraining.com for more information</a>.

                    </p>
                </div>
            <h2><u>Angel Shark Sponsors</u></h2>
            <!--
            <div class="col-lg-12">
                    <a target="_blank" href="https://www.endace.com/">
                        <img src="img/sponsors/endace_big.png">
                    </a>
                    <p><strong>Endace's</strong> 100% accurate, Network History Recording and Playback provides definitive, packet-level evidence for investigating cybersecurity threats, quantifying data breaches and troubleshooting network or application performance problems. Playback integrates with commercial, open-source or custom analytics applications to streamline and automate issue investigation. Network History can be played back through hosted or external analytics solutions for automated, back-in-time investigations. Global customers include banks, hospitals, telcos, broadcasters, retailers, web giants, governments and military.</p>
                </div> -->
            <div class="col-lg-12">
                    <a target="_blank" href="http://www.profitap.com/">
                        <img src="img/sponsors/profitap.png">
                    </a>
                    <p><strong>  PROFITAP</strong> designs and manufactures network visibility solutions for the security, forensics, deep packet capture, and network performance monitoring sectors. PROFITAP's extensive portfolio of copper and fiber TAPs, packet brokers, and field service troubleshooters, offers tailored solutions for network visibility and monitoring. With the release of the award-winning ProfiShark series, PROFITAP is providing a perfect packet capture solution for network analysis with Wireshark. Visit <a href="http://www.profitap.com">www.profitap.com</a> for more information.</p>
                </div>

            <h2><u>Group Packet Challenge Sponsor</u></h2>
                <div class="col-lg-12">
                    <a target="_blank" href="https://www.endace.com/">
                        <img src="img/sponsors/endace_big.png">
                    </a>
                    <p><strong>Endace's</strong> 100% accurate, Network History Recording and Playback provides definitive, packet-level evidence for investigating cybersecurity threats, quantifying data breaches and troubleshooting network or application performance problems. Playback integrates with commercial, open-source or custom analytics applications to streamline and automate issue investigation. Network History can be played back through hosted or external analytics solutions for automated, back-in-time investigations. Global customers include banks, hospitals, telcos, broadcasters, retailers, web giants, governments and military.</p> <br>
                </div>
            <h2 style=""><u>Honorary Sponsor</u></h2>
                <div class="col-lg-12 clear-both">
                    <a target="_blank" href="http://www.lovemytool.com/">
                        <img src="img/sponsors/lovemytool.png">
                    </a>
                    <p><strong>LoveMyTool</strong> is an Open Internet Community for Network Analysis, Monitoring and Management technology reviews. We review open source solutions to cyber evidence technology and articles from top network and protocol analysts and are sponsored by the finest companies in our industry!
                </div>
            
        </div>
    </div>
</div>


<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>