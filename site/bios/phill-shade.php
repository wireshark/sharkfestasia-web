<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/phill-shade.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Phill Shade,Senior Forensics Investigator, Merlion’s Keep Consulting</h2>
		<p>
		Phillip "Sherlock" Shade is the founder of Merlion’s Keep Consulting, a professional services company specializing in all aspects of network and forensics analysis and providing a full range of professional training and customized curriculum development. Phill is a certified instructor for Wireshark University and Global Knowledge. Drawing from his 30+ years of hands-on, real world experience in network analysis, troubleshooting and cyber forensics/ security, Phill's presentations use a highly energetic, knowledgeable and informative style. Phill can be contacted at <a href="mailto:phill.shade@gmail.com">phill.shade@gmail.com</a> or <a href="mailto:merlions.keep@gmail.com">merlions.keep@gmail.com</a>.</p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>