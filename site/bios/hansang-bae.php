<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/hansang.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Hansang Bae - CTO of Riverbed</h2>
		<p>
		Hansang Bae is Chief Technology Officer at Riverbed Technology, responsible for guiding Company's technology vision and strategy in the application performance industry, as well as SteelScript (open APIs) and Wireshark open source development. In his tenure with the company, Bae has held several executive positions including Chief Scientist leading the Strategic Technology Group of the CTO office and the Global Consulting Engineering team. Prior to joining Riverbed, Bae was a member of the Citi (Citigroup) Architecture and Technology Engineering leadership team. As one of the six global engineering leads, he was responsible for performance engineering, network management (NMS) tools, and capacity planning groups for all of Citi.</p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>