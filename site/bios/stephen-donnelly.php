<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/stephen-donnelly.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Stephen Donnelly - CTO of Endace</h2>
		<p>
		Stephen has worked on packet capture and time stamping systems for 20 years, receiving his PhD on “High Precision Timing in Passive Measurements of Data Networks” from the University of Waikato, New Zealand. He was a founding employee at Endace, developing FPGA based packet capture and timing systems. He has developed clock synchronization systems and high performance network monitoring virtualization for Endace appliances, and collaborated with customers in Telcos, Finance, Test & Measurement, Enterprise, and Government to solve unique problems. Stephen is a contributor to the Wireshark, libpcap, Argus and Suricata open source projects.</p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>