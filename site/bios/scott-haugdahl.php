<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/scott-haugdahl.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Scott Haugdahl, Architect, Blue Cross Blue Shield MN</h2>
		<p>
		Scott's analysis career started with developing PC-DOS-based packet analysis tools, evolved to designing expert systems, and cumulated with architecting high-end packet visibility fabrics for large capacity sniffers, security, and APM appliances. He has been a Wireshark enthusiast since Ethereal 0.99 or thereabouts! Outside of work Scott loves photography and travel.</p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>