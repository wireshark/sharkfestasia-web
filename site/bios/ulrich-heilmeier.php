<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/ulrich.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Ulrich Heilmeier - Network Architect, Krones AG</h2>
		<p>
		Uli Heilmeier has been a network protocol enthusiast for years. He believes in RFCs and sharing knowledge. Hunting packets is one of his favorite occupations while working as a network engineer at a German machine manufacturer. </p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>