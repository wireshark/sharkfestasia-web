<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/christian-landstrom.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Christian Landström - Senior Consultant @ Airbus Defence and Space</h2>
		<p>
		Christian Landström has worked in IT since 2004, with a strong focus on network communications and IT security. After graduating in computer science in 2008 and joining Synerity Systems directly afterwards he moved with the whole Synerity team to work for Fast Lane GmbH in 2009 as a Senior Consultant. Since 2013 he has been working as a Senior Consultant for Airbus Defence and Space CyberSecurity. He is a certified Cisco teacher as well as being an OSCP, working on IT security and network analysis projects.</p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>