<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/roland.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Roland Knall - Wireshark Core Developer</h2>
		<p>
		Roland is a Software System Architect for machine safety protocols at B&R Industrial Automation, a division of ABB. He started developing software some 20 years ago and has seen nearly all parts of software development, but focusing the last 10 years on industrial machine applications and mainly on systems in the area of industrial ethernet. He has been a Core Developer of Wireshark since 2016 and focuses mainly on the integration of external capture devices as well as UI improvements. </p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>