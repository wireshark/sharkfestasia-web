<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/hansang.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Mark Stout - Tech Support Engineer IV, Sprint</h2>
		<p>
		Mark has worked in the enterprise wireless space for over 19 years and has turned up Code Division Multiple Access (CDMA) and Long-Term Evolution (LTE) high-speed wireless networks around the world.  He is currently the Lead Support Engineer for Sprint’s LTE technology on the Evolved Packet Core (EPC) network, which is a framework for providing converged voice and data on a 4G LTE network</p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer-absolute.php"); ?>