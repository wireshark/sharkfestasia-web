<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/mike-kershaw.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Mike Kershaw - Wireless Hacker @ Kismet Wireless</h2>
		<p>
		Mike Kershaw is the author of Kismet, an open source wireless sniffer and IDS, various other open source software and hardware projects, as well as working in enterprise wireless and securing mobile devices.</p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>