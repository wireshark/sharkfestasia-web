<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/ron-henderson.jpg">
	</div>
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2>Ronald Henderson, CTO, UNIVERSAL Technologies</h2>
		<p>
		Ronald W. Henderson has more than 30 years' experience in various enterprise network computing environments including infrastructure, virtual computing, open source development, networks, systems, security architecture, data center design, deployment and management. As Chief Technology Officer (CTO) for UNIVERSAL Technologies, Inc. was lead architect and project head on numerous research and development initiatives which resulted in the creation of effective integrated enterprise solution products in the areas of enterprise server based and virtual desktop computing.</p>
		<p>Co-author of the Network Security Toolkit (NST) which is a Linux distribution that provides easy access to best-of-breed Open Source Network Security Applications and should run on most X86_64 platforms. NST was created in 2003 and includes a web-based frontend to the Wireshark command suite for both Single or Multi-Tap network packet capture and decode analysis.</p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>