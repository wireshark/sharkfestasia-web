<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/eduard-blenkers.jpeg">
	</div>
	<div class="col-sm-9">
		<h2>Eduard Blenkers - Sr. Network Consultant</h2>
		<p>
		Eduard "Eddi" Blenkers has analyzed countless networks and applications - often teaming up with Jasper Bongertz. Most analysis projects dumped SMB in his lap. The background in computer and network forensics often helped to link network packets to computer settings or misbehaving applications. Eddi is currently working in an incident response team chasing Windows-based malware. </p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>