<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/jon-ford.png">
	</div>
	<div class="col-sm-9">
		<h2>Jon Ford, Web Application Security Analyst, MainNerve LLC</h2>
		<p>
		Jon is a Web Application Security Analyst with MainNerve, Llc. He performs a variety of other jobs as well, from penetration testing of network systems, to wireless (802.11) exploitation to teaching personal cyber security and all the way up to doing dishes and taking out the trash. Jon has used Wireshark extensively in all of these jobs, minus the trash and dishes, but hopes to find a way to incorporate Wireshark into those duties as well one day. MainNerve LLC specializes in network and information security services and technology innovations. The company’s mission is to help organizations assess and manage risks associated with critical assets. MainNerve exists to fill the critical gap in cybersecurity technology and expertise for the SMB market by employing critical subject matter expertise and supporting that with state-of-the-art technologies and affordable solutions.</p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>