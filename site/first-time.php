<?php include "header-small.php"; ?>
<div class="col-lg-12 agenda-header why-attend">
	<div class="col-lg-5 agenda-header first-img">
		
	</div>
	<div class="col-lg-7 agenda-header-background first-background">
		<h1>New to SharkFest</h1>
			<h3>First-time attendee to-do’s and tips from past attendees to get you started.</h3>
			<!--<a target="_blank" href="/assets/SharkFest17EUAgenda.pdf">
				<button type="button" class="btn btn-primary btn-xl">Full Conference Session Agenda</button>
			</a>-->
		
		
	</div>
</div>
<div  id="about" class="col-lg-12 why-tabs-background">
	<div class="col-centered why-list-div">
		<ul class="first-list col-centered">
			<li><a href="#about">Welcome</a></li>
			<li><a href="#first-time">First Time Attendee?</li></a>
			<!--<li><a href="#tips">Tips from Past Attendees</li></a>-->
			<li><a href="#faq">Videos</li></a>
		</ul>
	</div>
</div>


<div class="container-fluid first-attend first-body">
	<div class="col-lg-12">
		<h2 id="first-time">WELCOME NEWCOMERS!</h2>
			<p>We hope you’ll check this page often for first-time attendee information to make your initial SharkFest experience easier.</p>
		<h2>First Time Attendee?</h2>
		<p>Whether you’re learning about SharkFest for the first time or are registered and don’t know what to do next, here’s your to do list:</p>
		<div class="reasons-spacing">
			<div class="col-md-6 col-sm-12 first-reasons">
				<div class="round-background">
					<h2>1</h2>
				</div>
				<div class="first-reason-text">
					<p><strong>Dress casually and in layers!</strong> SharkFest is an informal conference housed in venues that often ratchet-up the air conditioning to accommodate a mass of humanity in one space.  Dressing casually and bringing a coat or jacket will ensure your comfort while sitting in sessions or networking with peers at social events.</p>
				</div>
			</div>
		</div>
		<div class="reasons-spacing">
			<div class="col-md-6 col-sm-12 first-reasons">
				<div class="round-background">
					<h2>2</h2>
				</div>
				<div class="first-reason-text">
					<p><strong><a href="">Review and revisit the full conference agenda</a></strong> that will be posted soon a few times before the conference starts  and create a schedule of sessions you wish to attend.</p>
				</div>
			</div>
		</div>
		<div class="reasons-spacing">
			<div class="col-md-6 col-sm-12 first-reasons">
				<div class="round-background">
					<h2>3</h2>
				</div>
				<div class="first-reason-text">
					<p><strong>Follow us</strong> on <a href="https://twitter.com/SharkFest_2018"> Twitter</a> and <a href="https://www.facebook.com/SharkFest18/">Facebook</a>! </p>
				</div>
			</div>
		</div>
		<div class="reasons-spacing">
			<div class="col-md-6 col-sm-12 first-reasons">
				<div class="round-background">
					<h2>4</h2>
				</div>
				<div id="tips" class="first-reason-text">
					<p><strong>Download the SharkFest’18 ASIA App</strong>.  Familiarize yourself with the most current session agenda, speaker bios, social events, and more by downloading the Guidebook app and then searching for SharkFest’18 ASIA, which will be available 30 days before the conference start date.</p>
				</div>
			</div>
		</div>
		<div class="reasons-spacing">
			<div class="col-md-6 col-sm-12 first-reasons">
				<div class="round-background">
					<h2>5</h2>
				</div>
				<div id="tips" class="first-reason-text">
					<p><strong>Read the title AND the abstract of all SharkFest sessions of interest.</strong> Be sure the sessions you choose to attend cover the topics you’re interested in and download traces or other materials in advance of a session start time as requested in certain session abstracts.</p>
				</div>
			</div>
		</div>
		<div class="reasons-spacing">
			<div class="col-md-6 col-sm-12 first-reasons">
				<div class="round-background">
					<h2>6</h2>
				</div>
				<div id="tips" class="first-reason-text">
					<p><strong>Adjust your schedule onsite.</strong> There are no sign-up requirements or rules for attending specific sessions.  Just show up at a session at the assigned time and find a seat.  If the session turns out to be one that does not catch your fancy, feel free to go to another.  It’s your conference. Attend it your way.</p>
				</div>
			</div>
		</div>
		<div class="reasons-spacing">
			<div class="col-md-6 col-sm-12 first-reasons">
				<div class="round-background">
					<h2>7</h2>
				</div>
				<div id="tips" class="first-reason-text">
					<p><strong>Bring your laptop to all sessions.</strong>  Whether attending a hands-on session or lecture-based one, loading packet traces and accessing other resources provided by instructors on the fly could be important for a complete transfer of information.  Adequate power is available in all classroom spaces.</p>
				</div>
			</div>
		</div>
		<div class="reasons-spacing">
			<div class="col-md-6 col-sm-12 first-reasons">
				<div class="round-background">
					<h2>8</h2>
				</div>
				<div id="tips" class="first-reason-text">
					<p><strong>Socialize and make merry with new friends and contacts.</strong>  SharkFest is an informal, social experience aimed at bringing like-minded IT Professionals together to share Wireshark knowledge and experience.  Get to know as many of your peers and share as much information as possible during your stay!</p>
				</div>
			</div>
        </div>

	</div>
	<!--<div class="col-lg-3 col-md-12 about-gallery">
		<a style="font-size: 25px;" data-lightbox="sf16eu" alt="Click Here to View Pictures from Sharkfest '16 Europe!" href='img/sharkfest16eugallery/2.jpg'><img class="img-responsive" src="img/sharkfest16eugallery/sfeugal.jpg" ></a>
        <!--<img id="first"  class="img-responsive" src="img/estoril-vintage.jpg">
    </div>
                    <a href='img/sharkfest16eugallery/1.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/3.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/4.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/5.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/6.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/7.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/8.jpg' data-lightbox="sf16eu"></a>
                    
                    <a href='img/sharkfest16eugallery/9.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/10.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/11.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/12.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/13.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/14.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/15.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/16.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/17.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/18.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/19.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/20.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/21.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/22.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/23.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/24.jpg' data-lightbox="sf16eu"></a>
                    <a href='img/sharkfest16eugallery/25.jpg' data-lightbox="sf16eu"></a> -->
</div> <!--
<!--<div class="container-fluid first-attend first-alt-background-color first-body">
	<div class="col-sm-12">
		<h2>Tips from Past Attendees</h2>
		<ol class="first-tips-list">
			<li>Follow <a href="https://twitter.com/SharkFest_2017"> @SharkFest</a> and the #SharkFest17EU on Twitter</li>
			<li>Download the SharkFest Europe guide via Guidebook on your mobile phone</li>
			<li>Rerum delectus rerum enim autem odio velit id. Eos velit facere Rerum delectus rerum enim autem odio velit id. Eos velit facere</li>
			<li>Rerum delectus rerum enim autem odio velit id. Eos velit facere Rerum delectus rerum enim autem odio velit id. Eos velit facere</li>
			<li>Rerum delectus rerum enim autem odio velit id. Eos velit facere Rerum delectus rerum enim autem odio velit id. Eos velit facere</li>
			<li>Rerum delectus rerum enim autem odio velit id. Eos velit facere Rerum delectus rerum enim autem odio velit id. Eos velit facere</li>
		</ol>
	</div>
	
	<!--<div class="col-lg-3 first-body">
		<h2>New to SharkFest?</h2>
		<button><strong>Click here to go to our New to SharkFest page</strong></button>
	</div>
</div> -->
<div class="container-fluid first-body first-alt-background-color">
	<div class="col-lg-12">
		<h2 id="faq">Videos</h2>
			<div class="col-md-4">
				<div class="responsive-iframe2">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/oqxw3ePtZTM" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-md-4">
				<div class="responsive-iframe2">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/rLfYuO6pdVA" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-md-4">
				<div class="responsive-iframe2">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/WyVgEof2uKg" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
	</div>
</div>


<?php include  "footer.php"; ?>